package org.bestriped.switcheroo

import com.typesafe.config.{ConfigFactory, Config => TypesafeConfig}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ValueReader

/**
 * Class responsible for holding Switcheroo configuration.
 */
class Config(val people: Array[Person],
             val adjacentSeats: List[SeatRelation],
             val adjacentSeatCost: Double,
             _oppositeSeats: List[SeatRelation],
             val oppositeSeatCost: Double,
             val sameSeatCost: Double) {
  val oppositeSeats = _oppositeSeats ::: adjacentSeats
}

object Config {
  def apply(people: Array[Person],
            adjacentSeats: List[SeatRelation],
            adjacentSeatCost: Double,
            oppositeSeats: List[SeatRelation],
            oppositeSeatCost: Double,
            sameSeatCost: Double): Config = {
    new Config(people, adjacentSeats, adjacentSeatCost,
      oppositeSeats, oppositeSeatCost, sameSeatCost)
  }

  /**
   * Load config from the default TypeSafe Config configuration
   */
  def load(): Config = {
    val conf = ConfigFactory.load()
    load(conf)
  }

  /**
   * Load config based on a provided typesafe config.
   */
  def load(conf: TypesafeConfig): Config = {
    val people = (for (i <- conf.as[List[String]]("switcheroo.people"))
      yield Person(i)).toArray

    val adjacentSeats = conf.as[List[SeatRelation]]("switcheroo.adjacentSeats")
    val oppositeSeats = conf.as[List[SeatRelation]]("switcheroo.oppositeSeats")
    val adjacentSeatCost = conf.as[Double]("switcheroo.adjacentSeatCost")
    val oppositeSeatCost = conf.as[Double]("switcheroo.oppositeSeatCost")
    val sameSeatCost = conf.as[Double]("switcheroo.sameSeatCost")

    new Config(people, adjacentSeats, adjacentSeatCost,
               oppositeSeats, oppositeSeatCost, sameSeatCost)
  }

  /**
   * Implicit conversion for config allowing us to load config.as[List[SeatRelation]]
   */
  implicit val seatRelationConfigReader: ValueReader[List[SeatRelation]] = new ValueReader[List[SeatRelation]] {
    override def read(config: TypesafeConfig, path: String): List[SeatRelation] = {
      for (i <- config.as[List[List[Int]]](path)) yield SeatRelation(i(0), i(1))
    }
  }
}
