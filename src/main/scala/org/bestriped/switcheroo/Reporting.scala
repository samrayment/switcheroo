package org.bestriped.switcheroo

import scala.concurrent.Await
import scala.concurrent.duration._
import dispatch._, Defaults._

/**
 * Trait for a class capable of reporting a new chosen seating
 * arrangement.
 */
trait NewSeatingReporter {
  def reportNewSeatingArrangement(config: Config, newSeatingArrangement: List[Person])
}


class SlackSeatingReporter extends NewSeatingReporter {
  val slackUrl = "https://thisisglobal.slack.com/services/hooks/incoming-webhook?token=Ie4kiVrLaKY53HA4C4SM6Nge"

  override def reportNewSeatingArrangement(config: Config, newSeatingArrangement: List[Person]): Unit = {
    sendMessageToSlack(newSeatingArrangement.toString)
  }

  def sendMessageToSlack(message: String) {
    val request = url(slackUrl).POST
      .setContentType("application/json", "UTF-8") << "{\"text\": \"" + message +"\", \"username\": \"Switcheroo\"}"
    val httpResp = Http(request OK as.String)
    Await.result(httpResp, 5 seconds)
  }
}


class ConsoleSeatingReporter extends NewSeatingReporter {
  override def reportNewSeatingArrangement(config: Config, newSeatingArrangement: List[Person]) {
    println(newSeatingArrangement)
  }
}


object Reporting {
  def newSeatingReporter: NewSeatingReporter = {
    new ConsoleSeatingReporter
  }
}

