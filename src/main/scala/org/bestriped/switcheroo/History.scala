package org.bestriped.switcheroo

import com.fasterxml.jackson.core.JsonParseException
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonAST.JArray
import org.json4s.JsonDSL._
import org.bestriped.switcheroo.History.personToJson

/**
 * Exception thrown if the history json file has become corrupted
 * @param message
 */
case class HistoryCorruptException(message: String) extends Exception(message) {

}

/**
 * History management class responsible for parsing history
 * from json and updating it.  Note this class operates on
 * strings and file persistance is handled elsewhere.
 *
 */
class History {
  def toJson(history: List[Array[Person]]): String = {
    val historyRecordList = (for (historyRecord <- history) yield historyRecord.toList)
    compact(render(historyRecordList))
  }

  /**
   * Parse a json string into a List of History arrays.
   * @param jsonString The json string to parse
   * @return
   */
  def parseJson(jsonString: String): List[Array[Person]] =
    try {
      if (jsonString == "") {
        List()
      } else {
        val json = parse(jsonString)
        json match {
          case JArray(x:List[_]) => processHistory(x.asInstanceOf[List[JArray]])
          case _ => List()
        }
      }
    } catch {
      case ex: JsonParseException => throw HistoryCorruptException("JSON History is corrupt and cannot be loaded.")
    }

  def processHistory(historyJson: List[JArray]): List[Array[Person]] = {
    for(historyRecordJson <- historyJson)
      yield generateHistoryRecord(historyRecordJson)
  }

  def generateHistoryRecord(historyRecordJson: JArray):Array[Person] = {
    (for (JString(name) <- historyRecordJson) yield Person(name)).toArray
  }
}

object History {
  /* Implicit to marshall a person record to a JSON string. */
  implicit def personToJson(person: Person): JValue = JString(person.name)
}
