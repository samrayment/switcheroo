package org.bestriped.switcheroo

import akka.actor.Actor.Receive
import akka.actor.{ActorRef, ActorSystem, Props, Actor}
import akka.routing.RoundRobinRouter

import scala.util.Random

object ResultsCalculator {
  def apply() = new AkkaSeatCalculator
  //def apply() = new SimpleSeatCalculator
}

/**
 * Trait for objects calculating the optimum seating position using
 * the provided Heuristics.
 */
trait OptimalSeatCalculator {
  def calculateOptimalSeating(people: List[Person], heuristics: Heuristics): (List[Person], Double)
}

/**
 * Simple Seat Calculator that brute forces a certain number of shuffled
 * permuations in a simple loop.
 */
class SimpleSeatCalculator extends OptimalSeatCalculator {
  override def calculateOptimalSeating(people: List[Person], heuristics: Heuristics): (List[Person], Double) = {
    var currentCost = Double.MaxValue
    var chosenSeating: List[Person] = null;

    for (i <- 1 until 2000000) {
      val randomSeating = Random.shuffle(people)
      val cost = heuristics.score(randomSeating)
      if (cost < currentCost) {
        currentCost = cost
        chosenSeating = randomSeating
      }
    }
    (chosenSeating, currentCost)
  }
}

/**
 * Seat Calculator using Akka to generate results in parallel.
 */
class AkkaSeatCalculator extends OptimalSeatCalculator {
  override def calculateOptimalSeating(people: List[Person], heuristics: Heuristics): (List[Person], Double) = {
    val system = ActorSystem("mySystem")

    var chosenSeating: List[Person] = null
    var currentCost: Double = 0
    val heuristicCallback = (seating: List[Person], cost: Double) => {
      chosenSeating = seating
      currentCost = cost
    }
    val master = system.actorOf(
      Props(new HeuristicMaster(people, heuristics, heuristicCallback)),
      name="master")
    master ! Start()

    system.awaitTermination()
    (chosenSeating, currentCost)
  }
}

sealed trait SwitcherooAkkaMessage
case class Start() extends SwitcherooAkkaMessage
case class Work(startingGenerate: Int, people: List[Person], heuristics: Heuristics) extends SwitcherooAkkaMessage
case class Result(chosenSeating: List[Person], cost: Double) extends SwitcherooAkkaMessage

/**
 * Worker akka actor responsible for calculating the best seating
 * on a single thread.
 */
class HeuristicWorker extends Actor {
  def receive = {
    case Work(startingGenerator, people, heuristics) => {
      val (chosenSeating, currentCost) = generateBestSeating(people, heuristics)
      sender ! Result(chosenSeating, currentCost)
    }
  }

  def generateBestSeating(people: List[Person], heuristics: Heuristics): (List[Person], Double) = {
    var currentCost = Double.MaxValue
    var chosenSeating: List[Person] = null;
    for (i <- 1 until 500000) {
      val randomSeating = Random.shuffle(people)
      val cost = heuristics.score(randomSeating)
      if (cost < currentCost) {
        currentCost = cost
        chosenSeating = randomSeating
      }
    }
    (chosenSeating, currentCost)
  }
}

/**
 * Master akka responsible for creating worker actors and collating
 * results.
 *
 * Creates a number of workers and sends them each a Work() message,
 * it then receives a Result() message back from each Worker and shuts down
 * the actor system when all workers have returned.
 */
class HeuristicMaster(val people: List[Person],
                      val heuristics: Heuristics,
                      val numberOfWorkers: Int,
                      val resultReporter: ((List[Person], Double) => Unit)) extends Actor {

  def this(people: List[Person], heuristics: Heuristics,
           resultReporter: ((List[Person], Double) => Unit)) {
    this(people, heuristics, 4, resultReporter)
  }

  val workerRouter = context.actorOf(
    Props[HeuristicWorker].withRouter(RoundRobinRouter(numberOfWorkers)), name = "workerRouter")
  var chosenSeating: List[Person] = null
  var currentCost = Double.MaxValue
  var receivedResults = 0

  override def receive: Receive = {
    case Start() => {
      for (i <- 0 until numberOfWorkers) workerRouter ! Work(0, people, heuristics)
    }
    case Result(seating, cost) => {
      if (cost < currentCost) {
        currentCost = cost
        chosenSeating = seating
      }
      receivedResults += 1
      if (receivedResults == numberOfWorkers) {
        resultReporter(chosenSeating, currentCost)
        context.system.shutdown()
      }
    }
  }
}
