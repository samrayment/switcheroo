package org.bestriped.switcheroo

/**
 * Utility object containing functions that can be used throughout
 * the switcheroo.
 */
object Utils {
  /**
   * Open a file-like object, peform an operation on it and then
   * ensure that is closed afterwards.
   */
  def using[A <: {def close(): Unit}, B](param: A)(f: A => B): B =
    try { f(param) } finally { param.close() }
}
