package org.bestriped.switcheroo

/**
 * Class describing how seats relate to each other, wraps
 * two integers describing the two seats that relate to each
 * other.
 */
case class SeatRelation(val first: Int,
                        val second: Int) {
}

/**
 * A wrapper class around a string indicating a type for a person
 * who can occupy a seat
 *
 * @param name The name of the person.
 */
class Person(val name: String) {
  override def toString(): String = {
    return "Person(" + this.name + ")"
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[Person]

  override def equals(other: Any): Boolean = other match {
    case that: Person =>
      (that canEqual this) &&
        name == that.name
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(name)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}


object Person {
  def apply(name: String):Person = {
    new Person(name)
  }
}
