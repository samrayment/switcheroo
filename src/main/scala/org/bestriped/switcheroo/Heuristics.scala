package org.bestriped.switcheroo

import org.bestriped.switcheroo.Heuristics.PersonTupleMapping

/**
 * Heuristic scoring class, apply this class to a seat ordering to
 * calculate the cost of that ordering.
 */
class Heuristics(val sameSeatCostMap: Map[(Person, Int), Double],
                 val adjacentSeatCostMap: PersonTupleMapping,
                 val oppositeSeatCostMap: PersonTupleMapping,
                 val config:Config) {

  def score(seating:List[Person]):Double = {
    this.score(seating.toArray)
  }

  /**
   * Calculate the heuristic score for the provided seating.
   * @param seating The seating to calculate a heuristic for.
   * @return The heuristic costing for this seating
   */
  def score(seating:Array[Person]):Double = {
    var sameSeatCost:Double = 0
    for (positionPair <- seating.zipWithIndex) {
      sameSeatCost += sameSeatCostMap.getOrElse(positionPair, 0.toDouble)
    }

    val adjacentCost = calculateCostForTuplePositions(seating, config.adjacentSeats,
      adjacentSeatCostMap)
    val oppositeCost = calculateCostForTuplePositions(seating, config.oppositeSeats,
      oppositeSeatCostMap)

    sameSeatCost + adjacentCost + oppositeCost
  }

  /**
   * Calculate the heuristic score for 2 people sitting in relation to each other.
   * @param seating The seating to calculate the cost for
   * @param seatPositions A List of position tuples that show which seats are related to each other.
   * @param costMap A set of costs for two people sitting in relation to each other.
   */
  def calculateCostForTuplePositions(seating: Array[Person],
                                     seatPositions: List[SeatRelation],
                                     costMap: PersonTupleMapping): Double = {
    var cost:Double = 0
    for (SeatRelation(seat1, seat2) <- seatPositions) {
      val (person1, person2) = (seating(seat1), seating(seat2))

      if (person1.name <= person2.name) {
        cost += costMap.getOrElse((person1, person2), 0.toDouble)
      }
      else {
        cost += costMap.getOrElse((person2, person1), 0.toDouble)
      }
    }
    cost
  }
}


/**
 * Object responsible for creating a heurisitc function
 * for scoring potential seating positions.
 * Created by sam.rayment on 03/08/2014.
 */
object Heuristics {
  type PersonTupleMapping = Map[(Person, Person), Double]

  /**
   *  Generate a Heuristic object for the given Config and History Records
   */
  def generateHeuristics(config: Config, historyRecords: List[Array[Person]]): Heuristics = {
    val sameSeatCostMap = collection.mutable.Map[(Person, Int), Double]().withDefaultValue(0)
    val adjacentSeatCostMap = collection.mutable.Map[(Person, Person), Double]().withDefaultValue(0)
    val oppositeSeatCostMap = collection.mutable.Map[(Person, Person), Double]().withDefaultValue(0)

    for((historyRecord, numberOfIterationsAgo) <- historyRecords.zipWithIndex) {
      val sameSeatCost = config.sameSeatCost - config.sameSeatCost * numberOfIterationsAgo * 0.1
      for ((person, seatingPosition) <- historyRecord.zipWithIndex) {
        sameSeatCostMap((person, seatingPosition)) += sameSeatCost
      }

      val adjacentSeatCost = config.adjacentSeatCost - config.adjacentSeatCost * numberOfIterationsAgo * 0.1
      generatePairedSeatingCost(adjacentSeatCostMap, adjacentSeatCost,
        config.adjacentSeats, historyRecord)

      val oppositeSeatCost = config.oppositeSeatCost - config.oppositeSeatCost * numberOfIterationsAgo * 0.1
      generatePairedSeatingCost(oppositeSeatCostMap, oppositeSeatCost,
        config.oppositeSeats, historyRecord)
    }
    new Heuristics(sameSeatCostMap.toMap,
      adjacentSeatCostMap.toMap,
      oppositeSeatCostMap.toMap,
      config)
  }

  def generatePairedSeatingCost(costMap: collection.mutable.Map[(Person, Person), Double],
                                 seatingCost: Double, pairedSeats: List[SeatRelation],
                                 historyRecord: Array[Person]) {
    for (SeatRelation(seat1, seat2) <- pairedSeats) {
      val person1 = historyRecord(seat1)
      val person2 = historyRecord(seat2)
      if (person1.name <= person2.name) {
        costMap((person1, person2)) += seatingCost
      }
      else {
        costMap((person2, person1)) += seatingCost
      }
    }
  }
}
