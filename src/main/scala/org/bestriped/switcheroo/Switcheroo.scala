package org.bestriped.switcheroo

import java.io.{FileNotFoundException, FileWriter}
import akka.actor.{Props, ActorSystem, Actor}

import scala.util.Random
import scala.io.Source
import Heuristics.generateHeuristics

import org.bestriped.switcheroo.Utils.using


/**
 * Main running object for the switcheroo loads, existing history
 * and config and generates the next seating position.
 */
object Switcheroo {

  def main(args: Array[String]) {
    try {
      val (people, config) = loadConfig
      val historyRecords = loadHistoryFromJson
      val heuristics = generateHeuristics(config, historyRecords)

      var chosenSeating: List[Person] = generateBestSeating(people, heuristics)

      storeHistory(historyRecords, chosenSeating)
      reportResult(config, chosenSeating)
    } catch {
      case ex: HistoryCorruptException => {
        println("The history file has been corrupted.")
        System.exit(1)
      }
    }
  }

  def generateBestSeating(people: List[Person], heuristics: Heuristics): List[Person] = {
    val resultsCalculator = ResultsCalculator()
    val (chosenSeating, currentCost) = resultsCalculator.calculateOptimalSeating(people, heuristics)
    chosenSeating
  }

  /**
   * Load the config.
   * Currently this is defined in code, later it'll be loaded from JSON.
   * @return
   */
  def loadConfig: (List[Person], Config) = {
    val config = Config.load()
    (config.people.toList, config)
  }

  /**
   * Load the history of previous seatings from JSON.
   * @return
   */
  def loadHistoryFromJson: List[Array[Person]] = {
    val history = new History()
    try {
      val historyRecordsFromFile = for (i <- Source.fromFile("history.json").getLines().toList)
      yield history.parseJson(i)
      val historyRecords = historyRecordsFromFile.apply(0)
      historyRecords
    } catch {
      case ex: FileNotFoundException => {
        // No history file yet assume empty history
        List()
      }
    }
  }

  /**
   * Store the given historyRecords with the chosen seating back in the json
   * history file.
   */
  def storeHistory(historyRecords: List[Array[Person]], chosenSeating: List[Person]) {
    val newHistoryRecord = chosenSeating.toArray :: historyRecords
    val history = new History()
    using(new FileWriter("history.json")) {
      fileWriter => fileWriter.write(history.toJson(newHistoryRecord.slice(0, 10)))
    }
  }

  def reportResult(config: Config, chosenSeating: List[Person]) {
    val reporter = Reporting.newSeatingReporter
    reporter.reportNewSeatingArrangement(config, chosenSeating)
  }
}
