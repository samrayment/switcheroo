package org.bestriped.switcheroo.tests

import org.bestriped.switcheroo.{SeatRelation, Heuristics, Person, Config}
import org.scalatest.FunSpec

/**
 * Created by sam.rayment on 05/08/2014.
 */
class HeuristicsSpec extends FunSpec {
  describe("With the a Config object") {
    val SameSeatCost = 2.0f
    val AdjacentSeatCost = 1.0f
    val OppositeSeatCost = 0.5f

    /**
     * Build a six person config with a desk of 4 and 2 seats
     * next to each other like this:
     *
     * 0 | 1    4 | 5
     * 2 | 3
     */
    val devNames = List("Dev 1", "Dev 2", "Dev 3", "Dev 4", "Dev 5", "Dev 6")
    val devs = for (i <- devNames) yield Person(i)
    val config = Config(devs.toArray,
      List(SeatRelation(0, 1), SeatRelation(2, 3), SeatRelation(4, 5)), AdjacentSeatCost,
      List(SeatRelation(0, 2), SeatRelation(1, 3)), OppositeSeatCost, SameSeatCost)

    describe("With a Single History Record") {
      val historyRecord = (for (i <- List("Dev 6", "Dev 5", "Dev 4", "Dev 3", "Dev 2", "Dev 1"))
                           yield Person(i)).toArray
      val heuristicsUnderTest = Heuristics.generateHeuristics(config, List(historyRecord))

      it("Should mark the SameSeatCost for current seats") {
        val sameSeatCostMap = heuristicsUnderTest.sameSeatCostMap
        assert(sameSeatCostMap.size == 6)
        assert(sameSeatCostMap((Person("Dev 6"), 0)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 5"), 1)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 4"), 2)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 3"), 3)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 2"), 4)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 1"), 5)) == SameSeatCost)
      }

      it("Should update the AdjacentSeatCost for adjacent seats") {
        val adjacentSeatCostMap = heuristicsUnderTest.adjacentSeatCostMap
        assert(adjacentSeatCostMap.size == 3)
        assert(adjacentSeatCostMap((Person("Dev 5"), Person("Dev 6"))) == AdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 3"), Person("Dev 4"))) == AdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 1"), Person("Dev 2"))) == AdjacentSeatCost)
      }

      it("Should update the OppositeSeatCost for opposite seats") {
        val oppositeSeatCostMap = heuristicsUnderTest.oppositeSeatCostMap
        assert(oppositeSeatCostMap.size == 5)
        assert(oppositeSeatCostMap((Person("Dev 5"), Person("Dev 6"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 3"), Person("Dev 4"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 1"), Person("Dev 2"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 4"), Person("Dev 6"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 3"), Person("Dev 5"))) == OppositeSeatCost)
      }
    }

    describe("With a second level of history") {
      val historyRecord = (for (i <- List("Dev 6", "Dev 5", "Dev 4", "Dev 3", "Dev 2", "Dev 1"))
                            yield Person(i)).toArray
      val historyRecord1 = (for (j <- List("Dev 5", "Dev 4", "Dev 3", "Dev 2", "Dev 1", "Dev 6"))
                            yield Person(j)).toArray
      val heuristicsUnderTest = Heuristics.generateHeuristics(config, List(historyRecord, historyRecord1))
      val SecondRoundSameSeatCost = 1.8
      val SecondRoundAdjacentSeatCost = 0.9
      val SecondRoundOppositeSeatCost = 0.45

      it("Should mark the SameSeatCost for current seats") {
        val sameSeatCostMap = heuristicsUnderTest.sameSeatCostMap
        assert(sameSeatCostMap.size == 12)
        assert(sameSeatCostMap((Person("Dev 6"), 0)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 5"), 1)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 4"), 2)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 3"), 3)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 2"), 4)) == SameSeatCost)
        assert(sameSeatCostMap((Person("Dev 1"), 5)) == SameSeatCost)

        assert(sameSeatCostMap((Person("Dev 5"), 0)) == SecondRoundSameSeatCost)
        assert(sameSeatCostMap((Person("Dev 4"), 1)) == SecondRoundSameSeatCost)
        assert(sameSeatCostMap((Person("Dev 3"), 2)) == SecondRoundSameSeatCost)
        assert(sameSeatCostMap((Person("Dev 2"), 3)) == SecondRoundSameSeatCost)
        assert(sameSeatCostMap((Person("Dev 1"), 4)) == SecondRoundSameSeatCost)
        assert(sameSeatCostMap((Person("Dev 6"), 5)) == SecondRoundSameSeatCost)
      }

      it("Should update the AdjacentSeatCost for adjacent seats") {
        val adjacentSeatCostMap = heuristicsUnderTest.adjacentSeatCostMap
        assert(adjacentSeatCostMap.size == 6)
        assert(adjacentSeatCostMap((Person("Dev 5"), Person("Dev 6"))) == AdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 3"), Person("Dev 4"))) == AdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 1"), Person("Dev 2"))) == AdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 4"), Person("Dev 5"))) == SecondRoundAdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 2"), Person("Dev 3"))) == SecondRoundAdjacentSeatCost)
        assert(adjacentSeatCostMap((Person("Dev 1"), Person("Dev 6"))) == SecondRoundAdjacentSeatCost)
      }

      it("Should update the OppositeSeatCost for opposite seats") {
        val oppositeSeatCostMap = heuristicsUnderTest.oppositeSeatCostMap
        assert(oppositeSeatCostMap.size == 9)
        assert(oppositeSeatCostMap((Person("Dev 5"), Person("Dev 6"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 3"), Person("Dev 4"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 1"), Person("Dev 2"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 4"), Person("Dev 6"))) == OppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 3"), Person("Dev 5"))) == OppositeSeatCost + SecondRoundOppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 4"), Person("Dev 5"))) == SecondRoundOppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 2"), Person("Dev 3"))) == SecondRoundOppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 1"), Person("Dev 6"))) == SecondRoundOppositeSeatCost)
        assert(oppositeSeatCostMap((Person("Dev 2"), Person("Dev 4"))) == SecondRoundOppositeSeatCost)
      }
    }

    describe("With a Heuristic with set costings") {
      // Many of the config values don't matter after you've calculated the score
      val config = Config(Array(),
        List(SeatRelation(0, 1), SeatRelation(2, 3)), 0,
        List(SeatRelation(0, 2), SeatRelation(1, 3)), 0, 0)

      val sameSeatCosting = Map(
        (Person("Dev 1"), 0) -> 2.0,
        (Person("Dev 2"), 1) -> 2.0,
        (Person("Dev 3"), 2) -> 2.0,
        (Person("Dev 4"), 3) -> 2.0
      )
      val adjacentSeatCosting = Map(
        (Person("Dev 1"), Person("Dev 2")) -> 1.0,
        (Person("Dev 3"), Person("Dev 4")) -> 0.9,
        (Person("Dev 2"), Person("Dev 4")) -> 0.9
      )
      val oppositeSeatCosting = Map(
        (Person("Dev 1"), Person("Dev 2")) -> 0.5,
        (Person("Dev 2"), Person("Dev 3")) -> 0.4,
        (Person("Dev 1"), Person("Dev 4")) -> 0.4
      )

      val heuristicsUnderTest = new Heuristics(sameSeatCosting, adjacentSeatCosting, oppositeSeatCosting, config)

      it("Should calculate the correct costs for a seating position") {
        val seating = List(Person("Dev 4"), Person("Dev 2"), Person("Dev 1"), Person("Dev 3"))
        val resultingScore = heuristicsUnderTest.score(seating)

        // 2.0 + 0.9 + 0.4 + 0.4
        assert(resultingScore == 3.7)
      }
    }
  }
}
