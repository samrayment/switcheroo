package org.bestriped.switcheroo.tests

import org.bestriped.switcheroo.{HistoryCorruptException, Person, History}
import org.scalatest.FunSpec

/**
 * Created by sam.rayment on 02/08/2014.
 */
class HistorySpec extends FunSpec {
  describe("The History class") {
    val classUnderTest = new History()
    it("Should load people from a JSON String") {
      val jsonResult = classUnderTest.parseJson("""[["Dev 1", "Dev 2"], ["Dev 2", "Dev 1"]]""")

      assert(jsonResult.size == 2)
      assert(jsonResult.head(0) == Person("Dev 1"))
      assert(jsonResult.head(1) == Person("Dev 2"))
      assert(jsonResult.last(0) == Person("Dev 2"))
      assert(jsonResult.last(1) == Person("Dev 1"))
    }

    it("Should be able to load a blank history") {
      val jsonResult = classUnderTest.parseJson("")

      assert(jsonResult.size == 0)
    }

    it("Should fire an exception when loading a corrupt config") {
      intercept[HistoryCorruptException] {
        val jsonResult = classUnderTest.parseJson("""{a": ["Dev 1", "Dev 2"], "b": ["Dev 2", "Dev 1"]}""")
      }
    }
  }
}
