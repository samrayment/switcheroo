import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._

name := "Switcheroo"

version := "0.1"

scalaVersion := "2.11.2"

val json4sNative = "org.json4s" %% "json4s-native" % "3.2.10"
val json4sJackson = "org.json4s" %% "json4s-jackson" % "3.2.10"
val scalaTest = "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
val dispatch = "net.databinder.dispatch" %% "dispatch-core" % "0.11.2"
val typesafeConfig = "com.typesafe"   %  "config"         % "1.2.0"
val mockito = "org.mockito" % "mockito-all" % "1.9.5"
val ficus = "net.ceedubs" %% "ficus" % "1.1.1"
val akka = "com.typesafe.akka" %% "akka-actor" % "2.3.5"

libraryDependencies ++= List(json4sNative, json4sJackson, scalaTest, dispatch,
                             typesafeConfig, mockito, ficus, akka)

packageArchetype.java_application
