# Switcheroo #

Note: this is a Scala implementation of a slung together Python script I was using and is still in it's infancy; so at the moment it might not meet your exact use case.

### What is this repository for? ###

The Switcheroo is an automatic seat allocation algorithm to ensure that team members sit with different team members rather than at the same desk to increase the propagation of ideas within a development team.

### How do I get set up? ###

* First you'll need sbt (http://www.scala-sbt.org/).
* Clone the repo.
* You can then run the project with the initial test configuration `sbt run`
* Probably a good plan to run the unit tests too `sbt test`

### Building for a production run? ###

* We can build a standalone build of Switcheroo using `sbt stage`

### Configuring for my team. ###

Coming Soon.
